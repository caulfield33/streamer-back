import mongoose from 'mongoose';

const connection = `mongodb://${process.env.MONGO_URL}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;

export const conn = mongoose.connect(connection, {useNewUrlParser: true,  useUnifiedTopology: true });