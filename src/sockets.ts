import _ from 'lodash';
import {StreamRecordModel} from './models/streamRecord';
import {Streamer} from './streamer/streamer';

export class Sockets{

    constructor(wss) {

        wss.params = [];

        wss.route = (path, handler) => {
            if (!wss.routeMap) {
                wss.routeMap = {};
            }

            let parts = path.split('/');

            _.each(parts, (part, i) => {
                if (part[0] === ':') {
                    wss.params[i] = part.slice(1);
                }
            });

            wss.routeMap[path] = handler;
        };

        wss.on('connection', (ws, request) => {
            ws.params = ws.params || {};
            let url = request.url;
            let parts = url.split('/');
            parts = _.map(parts, (part, i) => {
                let paramName = wss.params[i];
                if (paramName) {
                    ws.params[paramName] = part;
                    return `:${paramName}`;
                }

                return part;
            });

            url = parts.join('/');
            if (wss.routeMap[url]) {
                wss.routeMap[url](ws, request);
            }
        });

        wss.route('/', (ws, request) => {
            let streamRecord;
            let startRecordingAt = Date.now();
            const actions = [];

            ws.on('message', async (message) => {
                message = JSON.parse(message);
                if (message.event && message.event === "init") {
                    const record = await StreamRecordModel.findOne({id: message.id});
                    console.log('init')

                    if (record) {
                        streamRecord = record;
                    } else {
                        if (message.sessionStorage && message.sessionStorage.streamerTsClient) {
                            delete message.sessionStorage.streamerTsClient
                        }

                        console.log('start recording')

                        streamRecord = new StreamRecordModel({
                            key: message.key,
                            url: message.url,
                            start: message.start,
                            width: message.width,
                            height: message.height,
                            userAgent: message.userAgent,
                            localStorage: message.localStorage,
                            sessionStorage: message.sessionStorage,
                            cookies: message.cookies,
                        })
                    }

                } else {
                    actions.push(message)
                }

            });

            ws.on('close', async () => {
                streamRecord.actions = streamRecord.actions.concat(actions);
                streamRecord.end = new Date(startRecordingAt + streamRecord.actions[streamRecord.actions.length - 1].time);
                streamRecord.duration = streamRecord.actions[streamRecord.actions.length - 1].time - streamRecord.actions[0].time;

                console.log('end recording')
                await streamRecord.save({checkKeys: false});
            });
        });


        wss.route('/recording', async (ws) => {
            let streamer;
            let streamRecord;

            ws.on('message', async (message) => {
                message = JSON.parse(message);

                console.log('message', message);

                if (message.command === "prepare") {
                    streamRecord = await StreamRecordModel.findById(message.recordingId);
                    streamer = new Streamer(streamRecord, message.streamId);
                    await streamer.openStreamWindow();

                    streamer.isDone.then(() => {
                        ws.send(JSON.stringify({done: true}))
                    });

                    ws.send(JSON.stringify({prepared: true}))
                }

                if (message.command === "pause") streamer.pause();

                if (message.command === "play") streamer.isStreaming ? streamer.play() : streamer.startActionStream()
            });

        });
    }
}
