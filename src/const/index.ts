export const toIgnore: string[] = [
    'font',
    'script',
    'document',
    'stylesheet',
    'image',
    'other'
];

export const iPhoneUserAgent = "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25";

export const allowedExt: string[] = [
    '.js',
    '.ico',
    '.css',
    '.png',
    '.jpg',
    '.woff2',
    '.woff',
    '.ttf',
    '.svg',
];
