import mongoose from 'mongoose';
const ObjectId = mongoose.Schema.Types.ObjectId;

export interface StreamRecord {
    _id: string;
    owner: any;
    localStorage: any;
    sessionStorage: any;
    cookies: any;
    duration: number;
    end: Date;
    start: Date;
    width: number;
    height: number;
    userAgent: string;
    url: string,
    actions: any[];
}

const streamRecordSchema = new mongoose.Schema({
    owner: {
        type: ObjectId,
        ref: 'Account'
    },
    duration: {
        type: Number
    },
    end: {
        type: Date
    },
    start: {
        type: Date
    },
    width: {
        type: Number
    },
    height: {
        type: Number
    },
    userAgent: {
        type: String
    },
    url: {
        type: String
    },
    localStorage: {
        type: Object
    },
    sessionStorage: {
        type: Object
    },
    cookies: {
        type: Object
    },
    actions: [
        {
            type: Object
        }
    ]

}, {timestamps: true});

export const StreamRecordModel = mongoose.model('StreamRecord', streamRecordSchema);
