import {StreamRequests} from './streamRequests';
import {StreamRecord} from '../models/streamRecord';
import {fromKeyCode, mouseAction, sleep} from './streamHelpers';
import {Page} from 'puppeteer';

export class StreamerActions extends StreamRequests {

    private page;
    private streamId: string;

    protected streamRecord: StreamRecord;

    constructor(page: Page, streamId: string, streamRecord: StreamRecord) {
        super();
        this.page = page;
        this.streamId = streamId;
        this.streamRecord = streamRecord;
    }

    async streamSetup(url: string): Promise<void> {

        const cookies = [];
        const domain = url.split('/').slice(0, 3).join('/');

        Object.keys(this.streamRecord.cookies).forEach((key) => {
            cookies.push({name: key, value: this.streamRecord.cookies[key], url: domain});
        });

        cookies.push({name: 'ChromeStreamer', value: '1', url: domain});
        await this.page.setCookie(...cookies);
        await this.page.setViewport({width: this.streamRecord.width, height: this.streamRecord.height});
        await this.page.goto(url);
        await sleep(500);

        const storageData = {
            localStorage: this.streamRecord.localStorage,
            sessionStorage: this.streamRecord.sessionStorage
        };

        await this.page.evaluate((id, width, height, storageData) => {

            Object.keys(storageData).forEach((storage) => {
                Object.keys(storageData[storage]).forEach((item) => {
                    window[storage].setItem(item, storageData[storage][item]);
                })
            });

            window.postMessage({command: 'stream', id: id, width: width, height: height}, "*");

        }, this.streamId, this.streamRecord.width, this.streamRecord.height, storageData);

    }

    public async doubleClick(action): Promise<void> {
        return this.page.mouse.click(action.x, action.y, {clickCount: 2})
    }

    public async tripleClick(action): Promise<void> {
        return this.page.mouse.click(action.x, action.y, {clickCount: 3})
    }

    public async keyDown(action): Promise<void> {
        let keyCode = fromKeyCode(action.key);

        return keyCode ? this.page.keyboard.down(keyCode) : Promise.resolve();
    }

    public async keyUp(action): Promise<void> {
        let keyCode = fromKeyCode(action.key);

        return keyCode ? this.page.keyboard.up(keyCode) : Promise.resolve();
    }

    public async mouseDown(action): Promise<void> {
        return this.page.mouse.down(mouseAction(action.button))
    }

    public async mouseUp(action): Promise<void> {
        return this.page.mouse.up(mouseAction(action.button))
    }

    public async mouseMove(action): Promise<void> {
        await this.page.mouse.move(action.x, action.y);

        return this.page.evaluate((size) => {
            if (!window['streamerCursor']) {
                window['streamerCursor'] = document.createElement('div');
                document.body.appendChild(window['streamerCursor']);
            }

            window['streamerCursor'].style = `
                padding: 10px;
                background: #59e8e875;
                border-radius: 50%;
                opacity: 0.7;
                border: 2px solid black;
                position:fixed;
                top:${size.y}px;
                left:${size.x}px;
                z-index:2147483638;`;
        }, {x: action.x, y: action.y});
    }

    public async scroll(action): Promise<void> {
        return this.page.evaluate((x, y) => {
            window.scrollTo(x, y);
        }, action.x, action.y);
    }

    public async resize(action): Promise<void> {
        return this.page.setViewport({width: action.width, height: action.height})
    }

    public async paste(action): Promise<void> {
        return this.page.evaluate((text) => {
            const startValue = document.activeElement['value'];
            let pastedValue = '';

            if (startValue === '') {
                pastedValue = text;
            } else {
                pastedValue = [
                    startValue.slice(0, document.activeElement['selectionStart']),
                    text,
                    startValue.slice(document.activeElement['selectionEnd'])
                ].join('');
            }
            document.activeElement.setAttribute('value', pastedValue)
        }, action.text)
    }

    public async autofill(action): Promise<void> {
        await this.page.evaluate(() => window['previousActiveElement'] = document.activeElement);

        await this.page.mouse.click(action.x, action.y);

        return this.page.evaluate((text) => {
            document.activeElement['value'] = text;
            if (window['previousActiveElement']) window['previousActiveElement'].focus();
        }, action.text)
    }

}
