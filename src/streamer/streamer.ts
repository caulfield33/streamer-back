import puppeteer from 'puppeteer';
import * as path from 'path';
import {StreamerActions} from './streamerActions';
import {StreamRecord} from '../models/streamRecord';
import {sleep} from './streamHelpers';

export class Streamer {

    private browser;
    private pauseResolve;
    private pausePromise: Promise<void>;

    private isDoneResolve: (value?: any) => void;

    private streamRecord: StreamRecord;
    private currentStreamActionTime = 0;
    private page;
    private streamerActions: StreamerActions;

    public streamId: string;
    public isStreaming = false;

    public isDone: Promise<boolean> = new Promise(resolve => this.isDoneResolve = resolve)


    constructor(streamRecord: StreamRecord, streamId: string) {
        this.streamRecord = streamRecord;
        this.streamId = streamId;
    }

    public async openStreamWindow(): Promise<void> {
        this.browser = await puppeteer.launch({
            headless: false,
            args: [
                '--enable-usermedia-screen-capturing',
                '--allow-http-screen-capture',
                '--auto-select-desktop-capture-source=ChromeStream',
                '--load-extension=' + path.join(__dirname, '../puppeteer_extensions'),
                '--disable-extensions-except=' + path.join(__dirname, '../puppeteer_extensions'),
                '--disable-infobars',
                `--window-size=${this.streamRecord.width},${this.streamRecord.height + 166}`,
                '--allow-insecure-localhost',
                // '--auto-open-devtools-for-tabs'
            ]
        });

        const pages = await this.browser.pages();
        this.page = pages[0];
        await this.page.setBypassCSP(true);
        await this.page.setUserAgent(this.streamRecord.userAgent);
        await this.page.setRequestInterception(true);

        this.streamerActions = new StreamerActions(this.page, this.streamId, this.streamRecord);

        this.page.on('request', this.streamerActions.requestHandler);

        await this.streamerActions.streamSetup(this.streamRecord.url)
    }

    public closeStream(): void {
        this.page.close()
    }

    public play(): void {
        this.pauseResolve();
        console.log(this.pauseResolve)
    }

    public pause(): void {
        this.pausePromise = new Promise((resolve => this.pauseResolve = resolve));
    }

    public async startActionStream(): Promise<void> {

        this.isStreaming = true;

        for (const streamAction of this.streamRecord.actions) {

            await sleep(Math.max(streamAction.time - this.currentStreamActionTime, 0));
            this.currentStreamActionTime = streamAction.time;

            try {
                const streamActions = {
                    dblclick: (action) => this.streamerActions.doubleClick(action),
                    tplclick: (action) => this.streamerActions.tripleClick(action),
                    keydown: (action) => this.streamerActions.keyDown(action),
                    keyup: (action) => this.streamerActions.keyUp(action),
                    mousedown: (action) => this.streamerActions.mouseDown(action),
                    mouseup: (action) => this.streamerActions.mouseUp(action),
                    mousemove: (action) => this.streamerActions.mouseMove(action),
                    touchcancel: (action) => Promise.resolve(),
                    touchstart: (action) => Promise.resolve(),
                    touchend: (action) => Promise.resolve(),
                    touchmove: (action) => Promise.resolve(),
                    wheel: (action) => Promise.resolve(),
                    autofill: (action) => this.streamerActions.autofill(action),
                    paste: (action) => this.streamerActions.paste(action),
                    resize: (action) => this.streamerActions.resize(action),
                    scroll: (action) => this.streamerActions.scroll(action)
                };

                if (streamActions[streamAction.event]) await streamActions[streamAction.event](streamAction);

                if (this.pauseResolve) await this.pausePromise;

            } catch (e) {
                console.log(e);
                this.closeStream();
                break;
            }
        }

        this.isDoneResolve();
        this.closeStream();
    }
}
