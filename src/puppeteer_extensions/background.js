/* global chrome */
chrome.runtime.onMessage.addListener((message, sender) => {
    if (message.command === 'stream') {
        let peer = new window.Peer({
            host: 'localhost',
            secure: false,
            path: '/myapp',
            port: 9432
        });

        chrome.desktopCapture.chooseDesktopMedia(['tab'], (streamId) => {
            navigator.mediaDevices.getDisplayMedia({
                audio: false,
                video: {
                    deviceId: streamId,
                    width: message.width,
                    height: message.height,
                    minFrameRate: 60
                }
            }).then((stream) => {


                peer.call(message.id, stream);
                peer.on('error', console.error);

                chrome.tabs.executeScript( { file: 'onPeerConnected.js' } );
            });
        });
    }
});
