import mongoose from 'mongoose';
const ObjectId = mongoose.Schema.Types.ObjectId;

export interface Account {
    _id: string;
    name: string;
    email: string;
    streamRecords: any[];
}

const accountSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: Date
    },
    password: {
        type: Date
    },
    streamRecords: [
        {
            type: ObjectId,
            ref: 'StreamRecord'
        }
    ]

}, {timestamps: true});

export const AcountModel = mongoose.model('Account', accountSchema);
