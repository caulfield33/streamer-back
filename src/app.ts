import * as bodyParser from 'body-parser';
import express from 'express';
import routes from './routes/index';
import {Request, Response} from 'express-serve-static-core';
import {allowedExt} from './const/index';
import * as path from 'path';
import cors from 'cors';
import * as https from 'https';
import * as fs from 'fs';
import expressWs from 'express-ws';
import WebSocket from 'ws';
import {Sockets} from './sockets';
import {conn} from './database';

class Server {

    public app;
    public server;

    public port = process.env.APP_PORT || 3000;

    public static createServer(): Server {
        return new Server();
    }

    constructor() {
        this.app = express();

        this.server = https.createServer({
            key: fs.readFileSync(process.env.SSL_KEY),
            cert: fs.readFileSync(process.env.SSL_CERT)
        }, this.app);

        expressWs(this.app);

        new Sockets(new WebSocket.Server({server: this.server}));

        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.raw({limit: '50mb'}));
        this.app.use(bodyParser.text({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({
            limit: '50mb',
            extended: true
        }));

        this.app.use(cors());

        this.app.use('/api', routes);

        this.app.use(express.static(path.join(__dirname, `../www`)));

        this.app.get('/', (request: Request, response: Response) => {
            if (allowedExt.filter(ext => request.url.indexOf(ext) > 0).length > 0) {
                response.sendFile(path.join(__dirname, `../www/${request.url}`));
            } else {
                response.sendFile(path.join(__dirname, `../www/index.html`));
            }
        });

        this.app.get('/test', (request: Request, response: Response) => {
            if (allowedExt.filter(ext => request.url.indexOf(ext) > 0).length > 0) {
                response.sendFile(path.join(__dirname, `../www/${request.url}`));
            } else {
                response.sendFile(path.join(__dirname, `../www/test.html`));
            }
        });


        conn
            .then(() => {
                this.server.listen(this.port);
                console.log(`app started at https://localhost:${this.port}`)
            })
            .catch((e) => {
                console.log(e)
            })
    }
}

Server.createServer();
