# Streamer
### Required software
* Docker
* docker-compose
* nodejs
### Installation

* Run docker containers
```
docker-compose up -d
```

* Install nodejs dependencies
```
npm i
```

* Generate self-signed certificate
There are a few possibilities to generate certificates.  
The simple way is to generate them with `openssl`.
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -outform PEM -keyout ssl/key.pem -outform PEM -out ssl/cert.pem
```

### Development

Run dev server
```
npm run dev
```
