import Router from 'express';
import {Streamer} from '../../streamer/streamer';
import {Request, Response} from 'express-serve-static-core';
import account from './account'
import {StreamRecordModel} from '../../models/streamRecord';

const router = Router();

router.use('/account', account);

router.post('/', async (request: Request, response: Response) => {

    const {streamId, recordId} = request.body;

    const streamRecord = await StreamRecordModel.findById(recordId);

    const streamer = new Streamer(streamRecord, streamId);

    await streamer.openStreamWindow();

    return response.status(200).json({ready: true})
});

export default router
