import {toIgnore} from '../const/index';
import {StreamRecord} from '../models/streamRecord';
import * as _ from 'lodash';
import {sleep} from './streamHelpers';

export class StreamRequests {

    protected streamRecord: StreamRecord;
    protected currentStreamActionTime;

    constructor() {

    }


    requestHandler(request) {
        // if (!toIgnore.includes(request.resourceType())) {
        //
        //     const url = request.url();
        //     const currentRequestIndex = _.findIndex(this.streamRecord.actions,
        //         (action) => action.event === 'request' && action.url === url);
        //
        //     if (currentRequestIndex > 0) {
        //         const currentRequest: any = {...this.streamRecord.actions[currentRequestIndex]};
        //         //TODO replace splice witch check get request after currentStreamActionTime
        //         this.streamRecord.actions.splice(currentRequestIndex, 1);
        //         const response = {
        //             status: currentRequest.st,
        //             headers: currentRequest.h,
        //             body: currentRequest.r
        //         };
        //
        //         return sleep(currentRequest.delay).then(() => {
        //             return request.respond(response);
        //         });
        //     }
        // } else {
        //     return request.continue();
        // }

        return request.continue();
    }
}
