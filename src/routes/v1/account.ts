import Router from 'express';
import {createAccount} from '../../controllers/account';

const router = Router();

router.post('/createUser', createAccount);

export default router
